## los lambda son funciones anonimas y se debe escribir en una sola línea.
def main():
    palindromo = lambda string: string == string[::-1]
    print(palindromo("ana"))

if __name__ == "__main__":
    main()